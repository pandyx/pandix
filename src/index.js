var mensajes = ['Hola, ¿cómo estás', 'Tean Iceberg', 'Dios Griego', '#OdiomasaZen']

const miDependencia = () => {
    const mesaje = mensajes[Math.floor(Math.random() * mensajes.length)];
    console.log(`\x1b[34m${mesaje}\x1b[89m`);
  }
  
  module.exports = {
    miDependencia
  };